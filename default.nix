with import <nixpkgs> {}; 
stdenv.mkDerivation {
  name = "echo-uws";
  buildInputs = [ cmake openssl zlib ];
  src = ./.;
}

