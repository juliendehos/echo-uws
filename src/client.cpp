#include <uWS.h>

#include <iostream>
using namespace std;

void clientOnConnection(uWS::WebSocket<uWS::CLIENT> *ws, uWS::HttpRequest req) 
{
  string str = "hello from client";
  cout << "to server: " << str << endl;
  ws->send(str.c_str(), str.size(), uWS::TEXT);
}

void clientOnMessage(uWS::WebSocket<uWS::CLIENT> *ws, char *message, size_t length,
    uWS::OpCode opCode) 
{
  cout << "from server:" << string(message, length) << endl;
  exit(-1);
}

int main() 
{
  uWS::Hub h;
  h.onConnection(clientOnConnection);
  h.onMessage(clientOnMessage);
  //h.connect("ws://echo.websocket.org:80", nullptr);
  h.connect("ws://localhost:3000", nullptr);
  h.run();
  return 0;
}

