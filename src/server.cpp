#include <uWS.h>

#include <iostream>
using namespace std;

void serverOnMessage(uWS::WebSocket<uWS::SERVER> *ws, char *message, 
    size_t length, uWS::OpCode opCode) 
{
  cout << "echo:" << string(message, length) << endl;
  ws->send(message, length, opCode);
}

int main() 
{
  uWS::Hub h;
  h.onMessage(serverOnMessage);
  h.listen(3000);
  h.run();
  return 0;
}

